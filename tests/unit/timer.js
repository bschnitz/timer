define(function (require) {
  var registerSuite = require('intern!object');
  var assert = require('intern/chai!assert');

  registerSuite({
    name: 'timer',

    interleaving_timers: function()
    {
      var synchronizer = this.async();

      var tester = synchronizer.callback(function(timearray){
        var timeline = [ 10, 8, 6, 8, 6, 6, 6, 4, 2, 0, -2, -4 ];
        timeline.forEach(function(time, i){
          assert.isBelow( Math.abs(time*1000 - timearray[i]), 100, 
            'the '+i+'th time mesurement should be roughly equal to ' +
              (time*1000)
          );
        });
      });

      requirejs.config({baseUrl : 'bower_components'});

      requirejs( ["bschnitz-timer/timer"], function(timer){

        console.log("\nrunning interleaving_timers test.\n");

        var timearray = [];
        var j = 0;
        var timer_2 = new timer.TimeoutTimer();
        timer_2.init( function(){
          console.log( "timer 2 timed out" );
        }, 12000 ).start();

        var i = 0;
        var timer_1 = new timer.IntervalTimer();
        timer_1.init( function(){
          timeleft = timer_2.get_time_left();
          console.log( "time left for timer 2: " + timeleft );
          timearray.push(timeleft);
          if( i == 2 ){
            timer_2.restart( 10000 );
            console.log( "timer 2 was restarted with different time." )
          }
          if( i == 4 ){
            timer_2.pause();
            console.log( "timer 2 was paused." )
          }
          if( i == 6 ){
            timer_2.start();
            console.log( "timer 2 was resumed." )
          }
          if( ++i > 11 ){
            timer_1.stop();
            console.log( "timer 1 was stopped.\n" )
            tester(timearray);
          }
        }, 2000 ).start();
      });
    },
  });
});

//function test2(){
//  console.log("\nRunning test2.");
//  var counter = new timer.TimeCounter();
//  counter.on_interval( function(){
//    console.log("Total runtime: " + counter.get_total_runtime() );
//  }, 2000 ).on_timeout( function(){
//    console.log("Done!");
//    counter.stop();
//    test3();
//  }, 10000 ).start();
//}
///* expected output:
//Running test2.
//Total runtime: 2002
//Total runtime: 4005
//Total runtime: 6007
//Total runtime: 8009
//Done!
//*/
//
//function test3(){
//  console.log("\nRunning test3.");
//  var counter = new timer.TimeCounter();
//  counter.on_interval( function(){
//    console.log("Total runtime: " + counter.get_total_runtime() );
//    var interval = this.get_interval();
//    this.restart_interval( interval*2 );
//  }, 125 ).on_timeout( function(){
//    console.log("Done!");
//    counter.stop();
//    test4();
//  }, 10000 ).start();
//}
///* expected output:
//Running test3.
//Total runtime: 125
//Total runtime: 377
//Total runtime: 877
//Total runtime: 1879
//Total runtime: 3882
//Total runtime: 7887
//Done!
//*/
//
//function test4(){
//  console.log("\nRunning test4.");
//  var i = 0;
//  var mod = 300;
//  (new timer.AutoTimeCounter()).auto_interval( function(){
//    if( this.get_interval() > 50 && mod !== 5 ){
//      i = 0;
//      mod = 5;
//    }
//    if( i % mod == 0 )
//      console.log( i + " Total runtime: " + this.get_total_runtime() );
//    i++;
//  } ).on_timeout_stop( 120000 ).start();
//}
///* expected output
//Running test4.
//0 Total runtime: 21
//300 Total runtime: 5142
//600 Total runtime: 10245
//900 Total runtime: 15345
//1200 Total runtime: 20447
//1500 Total runtime: 25547
//1800 Total runtime: 30649
//2100 Total runtime: 35749
//2400 Total runtime: 40851
//2700 Total runtime: 45951
//3000 Total runtime: 51053
//3300 Total runtime: 56153
//0 Total runtime: 60016
//5 Total runtime: 65021
//10 Total runtime: 70027
//15 Total runtime: 75033
//20 Total runtime: 80039
//25 Total runtime: 85047
//30 Total runtime: 90054
//35 Total runtime: 95059
//40 Total runtime: 100066
//45 Total runtime: 105072
//50 Total runtime: 110078
//55 Total runtime: 115085
//*/
//
//function test5(){
//  console.log("\nRunning test5.");
//  var i = 0;
//  var mod = 5;
//  (new timer.AutoTimeCounter()).set_is_countdown().auto_interval( function(){
//    if( this.get_interval() < 50 && mod !== 300 ){
//      i = 0;
//      mod = 300;
//    }
//    if( i % mod == 0 )
//      console.log( i + " Time left: " + this.get_time_left() );
//    i++;
//  } ).on_timeout_stop( 120000 ).start();
//}
///* expected output:
//Running test5.
//0 Time left: 119980
//5 Time left: 114973
//10 Time left: 109961
//15 Time left: 104954
//20 Time left: 99947
//25 Time left: 94941
//30 Time left: 89933
//35 Time left: 84924
//40 Time left: 79917
//45 Time left: 74909
//50 Time left: 69902
//55 Time left: 64895
//0 Time left: 59886
//300 Time left: 54752
//600 Time left: 49650
//900 Time left: 44550
//1200 Time left: 39448
//1500 Time left: 34348
//1800 Time left: 29246
//2100 Time left: 24146
//2400 Time left: 19044
//2700 Time left: 13944
//3000 Time left: 8842
//3300 Time left: 3742
//*/
//
//test();
//
//});
