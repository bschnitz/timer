define({
  suites: [ 'tests/unit/timer' ],

  // npm install -g requirejs
  loaders: {
    'host-node':    'requirejs',
    'host-browser': 'bower_components/requirejs/require.js'
  },

  // A regular expression matching URLs to files that should not be included in
  // code coverage analysis. Set to  to completely disable code coverage.
  excludeInstrumentation: /^(?:tests|node_modules|bower_components)\//
});
