// Timer - javascript object oriented way of creating timers
//
// Copyright (C) 2016  Benjamin Schnitzler
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

"use strict";

define( ["bschnitz-tools/tools"],  function(tools){

var Timer = (function()
{
  var open = tools.private_variable_accessor("OpenTimer");

  var status = {
    uninitialized :  0x1, ///< init function not yet called, not ready
    initialized   :  0x2, ///< ready to be started
    running       :  0x4, ///< started and still running
    paused        :  0x8, ///< started and paused
    timeout       : 0x10  ///< timeout occeured
  };

  /// @function Timer
  ///     constructor of the Timer Class
  var Timer = function()
  {
    tools.privatize( {
      action: {
        set   : undefined,
        clear : undefined
      },
      timerid:   undefined,
      handle:    undefined,
      duration:  undefined,
      status:    status.uninitialized,
      starttime: undefined, ///< last time the timer status changed to 'running'
      totaltime: undefined
    }, this, open );
  };

  /// @function Timer.init
  ///     initialize the Timer object before usage
  ///
  /// @param function $set
  ///     the function to set the timer
  /// @param function $clear
  ///     the function to clear the timer (clearInterval/clearTimeout)
  /// @param function $handle
  ///     the callback parameter for the timer function
  /// @param integer $duration
  ///     the milliseconds argument for the timer function
  ///
  /// @return Timer this
  Timer.prototype.init = function( clear, handle, duration )
  {
    this.stop();

    if( handle === null || typeof(handle) === 'undefined' )
      handle = open(this).handle;

    if( duration === null || typeof(duration) === 'undefined' )
      duration = open(this).duration;

    open(this).handle       = handle;
    open(this).duration     = duration;
    open(this).action.set   = this.get_set_timer( handle, duration );
    open(this).action.clear = function(id){ clear(id); };

    open(this).status       = status.initialized;

    return this;
  }

  Timer.prototype.is_initialized = function()
  { return (open(this).status & status.initialized) != 0; }

  Timer.prototype.is_running = function()
  { return (open(this).status & status.running) != 0; }

  Timer.prototype.is_paused = function()
  { return (open(this).status & status.paused) != 0; }

  Timer.prototype.is_timed_out = function()
  { return (open(this).status & status.timeout) != 0; }

  Timer.prototype.get_starttime = function()
  { return open(this).starttime; }

  Timer.prototype.get_handle = function()
  { return open(this).handle; }

  Timer.prototype.get_difftime = function()
  { return this.is_running() ? Date.now() - this.get_starttime() : 0 }

  Timer.prototype.get_total_runtime = function()
  { return open(this).totaltime + this.get_difftime(); }

  Timer.prototype.get_duration = function()
  { return open(this).duration }

  /// @function Timer.start
  ///     start/resume the timer
  Timer.prototype.start = function()
  {
    var m = open(this);
    if( this.is_initialized() && ! this.is_running() )
    {
      if( ! this.is_paused() )
        open(this).totaltime = 0;
      m.starttime = Date.now();
      m.status = status.initialized | status.running;
      m.timerid = m.action.set();
    }
    return this;
  }

  /// @function Timer.restart
  ///     restart an already initialized Timer
  ///     resets the timer before restarting it.
  ///
  /// @param function $handle
  ///     the callback parameter for the timer function
  ///     if null/undefined, the previously defined value will be used
  /// @param integer $duration
  ///     the milliseconds argument for the timer function
  ///     if null/undefined, the previously defined value will be used
  ///
  /// @return Timer this
  Timer.prototype.restart = function( duration, handle )
  {
    this.init( handle, duration );
    return this.start();
  }

  /// @function Timer.pause
  ///     pause an already initialized Timer
  ///     does not reset the timer. the timer may be resumed with start().
  ///
  /// @param function $handle
  ///     the callback parameter for the timer function
  ///     if null/undefined, the previously defined value will be used
  /// @param integer $duration
  ///     the milliseconds argument for the timer function
  ///     if null/undefined, the previously defined value will be used
  ///
  /// @return Timer this
  Timer.prototype.pause = function()
  {
    if( this.is_running() )
    {
      open(this).action.clear( open(this).timerid );
      open(this).totaltime = this.get_total_runtime();
      open(this).status    = status.initialized | status.paused;
    }
    return this;
  }

  /// @function Timer.stop
  ///     reset the timer (can be restarted with Timer.start)
  Timer.prototype.stop = function()
  {
    if( this.is_running() )
      open(this).action.clear( open(this).timerid );
    open(this).starttime = undefined;
    open(this).totaltime = 0;
    open(this).status = status.initialized;
    return this;
  }

  /// @function Timer.sleep
  ///     block the JavaScript thread for the specified timespan
  /// @param int $milliseconds
  ///     the time to sleep
  Timer.sleep = function( milliseconds )
  {
    var starttime = new Date().getTime();

    while( starttime + milliseconds >= new Date().getTime() ) ;
    return this;
  }

  return Timer;
})();

var IntervalTimer = (function()
{
  var IntervalTimer = function(){
    // call the base class constructor on this object
    Timer.call( this );
  };

  // inherit from Class
  tools.extend( Timer, IntervalTimer );
  var parent = Timer.prototype;

  /// @function IntervalTimer.init
  ///     initialize the Timer object before usage
  ///
  /// @param function $handle
  ///     the callback executed on the end of each interval
  /// @param integer $duration
  ///     interval time in milliseconds
  ///
  /// @return IntervalTimer this
  IntervalTimer.prototype.init = function( handle, duration )
  { return parent.init.call( this, clearInterval, handle, duration ); }

  /// @function IntervalTimer.get_set_timer
  ///     internally used polymorpic function to start the timer
  ///     this function is meant to be private
  ///
  /// @param function $handle
  ///     the callback function executed on timeout
  /// @param integer $duration
  ///     the interval time
  ///
  /// @return function which starts the interval
  IntervalTimer.prototype.get_set_timer = function( handle, duration )
  { return function(){ return setInterval( handle, duration ) } }

  return IntervalTimer;
})();

var TimeoutTimer = (function()
{
  var TimeoutTimer = function(){
    // call the base class constructor on this object
    Timer.call( this );
  };

  // inherit from Class
  tools.extend( Timer, TimeoutTimer );
  var parent = Timer.prototype;

  TimeoutTimer.prototype.get_time_left = function()
  { return this.get_duration() - this.get_total_runtime(); }

  /// @function TimeoutTimer.init
  ///     initialize the Timer object before usage
  ///
  /// @param function $handle
  ///     the callback function executed on timeout
  /// @param integer $duration
  ///     timeout time in milliseconds
  ///
  /// @return IntervalTimer this
  TimeoutTimer.prototype.init = function( handle, duration )
  { return parent.init.call( this, clearTimeout, handle, duration ); }

  /// @function TimeoutTimer.get_set_timer
  ///     internally used polymorpic function to start the timer
  ///     this function is meant to be private
  ///
  /// @param function $handle
  ///     the callback function executed on timeout
  /// @param integer $duration
  ///     this argument ist ignored
  ///
  /// @return function which starts setTimeout
  TimeoutTimer.prototype.get_set_timer = function( handle, duration )
  { 
    var timer = this;
    return function(){ return setTimeout(handle, timer.get_time_left()); }
  }

  return TimeoutTimer;
})();

var TimeCounter = (function()
{
  var open = tools.private_variable_accessor("OpenTimeCounter");

  var TimeCounter = function(){
    tools.privatize( {
      timers : {
        interval : new IntervalTimer(),
        timeout  : new TimeoutTimer()
      }
    }, this, open );
  };

  function wrap_handle( me, handle, is_timeout )
  { return handle.bind( me, is_timeout ); }

  /// @function TimeCounter.init
  ///     initialize the TimeCounter object before usage
  ///
  /// @param function $handle( is_timeout )
  ///     this function is executed on timeout and on interval
  ///     (handle parameter - is_timeout - true if invoked on timeout)
  /// @param integer $timeout
  ///     timeout in milliseconds
  /// @param integer $interval
  ///     interval in milliseconds
  TimeCounter.prototype.init = function( handle, timeout, interval )
  {
    var timers = open(this).timers;
    timers.timeout.init ( wrap_handle( this, handle, true  ),  timeout );
    timers.interval.init( wrap_handle( this, handle, false ), interval );
  }

  /// @function on_timeout
  ///     set the timeout handle
  ///
  /// @param function $handle
  ///     the handle executed on timeout
  /// @param int $timeout
  ///     time after which the timeout handle is executed
  ///
  /// @return TimeCounter this
  TimeCounter.prototype.on_timeout = function( handle, timeout )
  {
    var timers = open(this).timers;
    timers.timeout.init ( wrap_handle( this, handle, true  ),  timeout );
    return this;
  }

  TimeCounter.prototype.on_interval = function( handle, interval )
  {
    var timers = open(this).timers;
    timers.interval.init( wrap_handle( this, handle, false ), interval );
    return this;
  }

  TimeCounter.prototype.get_interval = function()
  {
    var timers = open(this).timers;
    return timers.interval.get_duration();
  }

  TimeCounter.prototype.restart_interval = function( duration, handle )
  {
    var timers = open(this).timers;
    return timers.interval.restart( duration, handle );
  }

  TimeCounter.prototype.start = function()
  {
    open(this).timers.timeout.start();
    open(this).timers.interval.start();
  }

  TimeCounter.prototype.pause = function()
  {
    open(this).timers.timeout.pause();
    open(this).timers.interval.pause();
  }

  TimeCounter.prototype.stop = function()
  {
    open(this).timers.timeout.stop();
    open(this).timers.interval.stop();
  }

  TimeCounter.prototype.is_interval_running = function()
  { return open(this).timers.interval.is_running(); }

  TimeCounter.prototype.get_total_runtime = function()
  { return open(this).timers.timeout.get_total_runtime(); }

  TimeCounter.prototype.get_time_left = function()
  { return open(this).timers.timeout.get_time_left(); }

  return TimeCounter;
})();

var AutoTimeCounter = (function()
{
  var time_interval_length = 60;
  var milliseconds_per_second = 1000;
  var start_interval = milliseconds_per_second / time_interval_length;

  var type = {
    countdown :  0x1, ///< timer counts up
    countup   :  0x2  ///< timer counts down
  };

  var open = tools.private_variable_accessor("OpenAutoTimeCounter");

  var AutoTimeCounter = function()
  {
    tools.privatize( { type : type.countup }, this, open );
    TimeCounter.call( this );
  };

  tools.extend( TimeCounter, AutoTimeCounter );
  var parent = TimeCounter.prototype;

  AutoTimeCounter.prototype.set_is_countdown = function()
  {
    if( open(this).type !== type.countdown )
    {
      open(this).type = type.countdown;
      if( this.is_interval_running() )
        this.auto_set_interval();
    }
    return this;
  }

  AutoTimeCounter.prototype.set_is_countup = function()
  {
    if( open(this).type !== type.countup )
    {
      open(this).type = type.countup;
      if( this.is_interval_running() )
        this.auto_set_interval();
    }
    return this;
  }

  AutoTimeCounter.prototype.is_countdown = function()
  { return (open(this).type & type.countdown) !== 0; }

  AutoTimeCounter.prototype.is_countup = function()
  { return (open(this).type & type.countup) !== 0; }

  function get_interval_length( absolute_time_length )
  {
    var interval_length = start_interval;

    var time = absolute_time_length /
      (time_interval_length * milliseconds_per_second);

    while( time > 1 )
    {
      interval_length = interval_length * time_interval_length;
      time = time / time_interval_length;
    }

    return interval_length;
  }

  AutoTimeCounter.prototype.auto_set_interval = function()
  {
    var absolute_time_length = this.is_countup() ?
      this.get_total_runtime() : this.get_time_left();

    var interval_length = get_interval_length( absolute_time_length );

    if( interval_length !== this.get_interval() )
      this.restart_interval( interval_length );
  }

  function wrap_interval_handle( me, handle )
  {
    return function( is_timeout ){
      me.auto_set_interval();
      handle.call( me, is_timeout );
    }
  }

  AutoTimeCounter.prototype.auto_interval = function( handle )
  {
    var wrapped = wrap_interval_handle( this, handle );
    return parent.on_interval.call( this, wrapped, start_interval );
  }

  AutoTimeCounter.prototype.on_timeout_stop = function( duration )
  { return this.on_timeout( function(){ this.stop(); }, duration ); }

  return AutoTimeCounter;
})();

return {
  IntervalTimer   : IntervalTimer,
  TimeoutTimer    : TimeoutTimer,
  TimeCounter     : TimeCounter,
  AutoTimeCounter : AutoTimeCounter
}

});
