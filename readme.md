## Testing

1. install intern and requirejs using npm
    - npm install -g intern
    - npm install -g requirejs
2. run the tests using intern
    - intern-client config=tests/intern
